# ResxXlsxConv

多言語対応リソース編集支援ツールです。

テキストリソースのみxlsxに抽出します。また、xlsxからテキストリソースに書き戻します。

### resx to xlsx ###

    > ResxXlsxConv -to-xlsx Foobar/Resources.resx Resources.xlsx


resxをxlsxに変換します。Resources.resxと同じ場所にある多言語リソース(Resources.ja-JP.resx等)も自動で検索され１つのテーブルに変換されます。  
変換されたエクセルは以下のようになります。

$Key | $.default | $.ja-JP | $Comment
-----|-----------|---------|-----------
KEY1 |Hello      |こんにちは|コメント
KEY2 |World      |世界      |

$.defaultは標準リソース、$.ja-JPは多言語リソースです。$Commentは標準リソースのコメントです。

### xlsx to resx ###

    > ResxXlsxConv -to-resx [-trunc] Foobar/Resources.resx Resources.xlsx

xlsxをresxに反映させます。  
標準リソース(Resources.resx)への書き戻しは、なるべくもとのリソースを破壊しないよう変更箇所のみ上書きします。例えば、画像リソース等はそのまま維持されます。多言語リソースは空欄でない項目のみで新規作成されます。
-truncオプションを指定した場合は、既存の項目を削除した新しいリソースを作成します。
