﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResxXlsxConv
{
    public class MainProcess
    {
        public void Execute(string[] args)
        {
            // 引数解析
            Option option;
            try
            {
                option = new Option(args);
            }
            catch(Exception)
            {
                Option.DumpHelp();
                Console.WriteLine("----");
                throw;
            }

            // コンバート
            if (option.ConvertMode == CovnertMode.ResxToXlsx)
            {
                Console.WriteLine("RESX to XLSX ...");
                var converter = ResourceMap.LoadResx(option.ResxFileName);
                converter.SaveXlsx(option.XlsxFileName);
            }
            else
            {
                Console.WriteLine("XLSX to RESX ...");
                var converter = ResourceMap.LoadXlsx(option.XlsxFileName);
                converter.SaveResx(option.ResxFileName, option.Truncate);
                if (option.IsDesigner)
                {
                    converter.SaveResourceDesigner(option.ResxFileName, option.DesignerNameSpace, option.Truncate);
                }

            }

            Console.WriteLine("done.");
        }
    }

}
