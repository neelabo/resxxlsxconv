﻿using System;
using System.IO;

namespace ResxXlsxConv
{
    /// <summary>
    /// 変換モード
    /// </summary>
    public enum CovnertMode
    {
        ResxToXlsx,
        XlsxToResx
    }

    /// <summary>
    /// コマンドライン引数
    /// </summary>
    public class Option
    {
        //
        public string ResxFileName { get; set; }

        //
        public string XlsxFileName { get; set; }

        //
        public CovnertMode ConvertMode { get; set; }

        //
        public bool Truncate { get; set; }

        //
        public bool IsDesigner { get; set; }
        public string DesignerNameSpace { get; set; }

        //
        public Option(string[] args)
        {
            for (int index = 0; index < args.Length; ++index)
            {
                var arg = args[index];

                if (arg.StartsWith("-"))
                {
                    switch (arg)
                    {
                        case "-to-resx":
                            ConvertMode = CovnertMode.XlsxToResx;
                            break;

                        case "-to-xlsx":
                            ConvertMode = CovnertMode.ResxToXlsx;
                            break;

                        case "-trunc":
                            Truncate = true;
                            break;

                        default:
                            if (arg.StartsWith("-designer:"))
                            {
                                IsDesigner = true;
                                var tokens = arg.Split(':');
                                DesignerNameSpace = tokens[1];
                            }
                            else
                            {
                                throw new ArgumentException($"対応していないオプションです。: {arg}");
                            }
                            break;
                    }
                }
                else
                {
                    switch (Path.GetExtension(arg).ToLower())
                    {
                        case ".resx":
                            this.ResxFileName = arg;
                            break;
                        case ".xlsx":
                            this.XlsxFileName = arg;
                            break;
                        default:
                            throw new FormatException($"対応していない拡張子のファイルです。: {arg}");
                    }
                }
            }

            if (this.ConvertMode == CovnertMode.ResxToXlsx)
            {
                if (this.ResxFileName == null) throw new ArgumentNullException($"RESX入力ファイルが必要です。");
                this.XlsxFileName = this.XlsxFileName ?? Path.ChangeExtension(this.ResxFileName, ".xlsx");
            }
            else
            {
                if (this.XlsxFileName == null) throw new ArgumentNullException($"XLSX入力ファイルが必要です。");
                this.ResxFileName = this.ResxFileName ?? Path.ChangeExtension(this.XlsxFileName, ".resx");
            }
        }

        //
        public static void DumpHelp()
        {
            Console.WriteLine("Usage: ResxXlsxConv.exe [options] files...");
            Console.WriteLine("");
            Console.WriteLine("Options:");
            Console.WriteLine("    -to-xlsx    .resx を .xlsx に変換します。(default)");
            Console.WriteLine("    -to-resx    .xlsx を .resx に変換します。");
            Console.WriteLine("    -trunc       .resx変換時に既存の項目を削除します。");
            Console.WriteLine("    -designer:<namespace>  .resx変換時に Resources.Designer.cs も出力します。");
            Console.WriteLine("");
        }
    }
}
