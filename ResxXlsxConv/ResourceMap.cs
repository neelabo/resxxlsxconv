﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Resources;
using System.IO;
using System.Collections;
using System.ComponentModel.Design;
using ClosedXML.Excel;
using Microsoft.CSharp;
using System.CodeDom;
using System.Resources.Tools;
using System.CodeDom.Compiler;

namespace ResxXlsxConv
{
    /// <summary>
    /// リソースレコード
    /// </summary>
    public class ResourceRecord
    {
        public string Comment { get; set; }
        public Dictionary<string, string> Values { get; set; } = new Dictionary<string, string>();
    }

    /// <summary>
    /// リソースマップ
    /// </summary>
    public class ResourceMap
    {
        public const string DefaultCulture = "default";

        private List<string> _cultures = new List<string>();
        private Dictionary<string, ResourceRecord> _records = new Dictionary<string, ResourceRecord>();

        //
        public static ResourceMap LoadResx(string filename)
        {

            if (!File.Exists(filename)) throw new FileNotFoundException();

            // 多言語リソース収集
            var path = Path.GetFullPath(filename);
            var dir = Path.GetDirectoryName(path);
            var name = Path.GetFileNameWithoutExtension(path);
            var ext = Path.GetExtension(path);
            var files = Directory.GetFiles(dir, $"{name}.*{ext}");

            var map = new ResourceMap();
            map._cultures = new List<string>() { ResourceMap.DefaultCulture }.Concat(files.Select(e => Path.GetExtension(Path.GetFileNameWithoutExtension(e)).Trim('.'))).ToList();

            // リソース読み込み
            foreach (var culture in map._cultures)
            {
                map.LoadResx(GetResxFilename(filename, culture), culture);
            }

            return map;
        }

        //
        private static string GetResxFilename(string filename, string culture)
        {
            return culture == ResourceMap.DefaultCulture ? filename : Path.ChangeExtension(filename, $".{culture}.resx");
        }

        //
        private void LoadResx(string filename, string culture)
        {
            Console.WriteLine($"Load {filename}");

            var nodes = LoadResxNodes(filename);

            foreach (var node in nodes)
            {
                // 文字列リソースのみ抽出
                var typename = node.GetValueTypeName((ITypeResolutionService)null).Split(',').First();
                if (typename != typeof(string).ToString()) continue;

                // レコード登録登録
                if (!_records.ContainsKey(node.Name))
                {
                    // 標準リソースのキーのみ有効
                    if (culture == DefaultCulture)
                    {
                        _records.Add(node.Name, new ResourceRecord());
                    }
                    else
                    {
                        continue;
                    }
                }

                var record = _records[node.Name];
                record.Values[culture] = node.GetValue((ITypeResolutionService)null) as string;

                // 標準リソースのみコメント抽出
                if (culture == DefaultCulture)
                {
                    record.Comment = node.Comment;
                }
            }
        }


        //
        public static ResourceMap LoadXlsx(string filename)
        {
            Console.WriteLine($"Load {filename}");

            var map = new ResourceMap();

            using (var stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (var workbook = new XLWorkbook(stream))
            {
                var worksheet = workbook.Worksheet("$Resources");

                // header
                var headers = new Dictionary<string, int>();

                for (int col = 1; ; ++col)
                {
                    var row = 1;
                    var cell = worksheet.Cell(row, col).GetString();
                    if (string.IsNullOrWhiteSpace(cell)) break;

                    headers.Add(cell, col);
                }

                // cultures
                map._cultures = headers.Keys.Where(e => e.StartsWith("$.")).Select(e => e.Substring(2).Trim()).ToList();

                for (int row = 2; ; ++row)
                {
                    var key = worksheet.Cell(row, headers["$Key"]).GetString()?.Replace("\r", "")?.Trim();
                    if (string.IsNullOrWhiteSpace(key)) break;

                    var record = new ResourceRecord();
                    foreach (var culture in map._cultures)
                    {
                        var cell = worksheet.Cell(row, headers["$." + culture]).GetString()?.Replace("\r", "")?.Trim();
                        record.Values[culture] = cell;
                    }

                    var comment = worksheet.Cell(row, headers["$Comment"]).GetString()?.Replace("\r", "")?.Trim();
                    record.Comment = comment;

                    try
                    {
                        map._records.Add(key, record);
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException(ex.Message + $" ({key})", ex);
                    }
                }
            }

            return map;
        }

        //
        public void SaveXlsx(string filename)
        {
            Console.WriteLine($"Save {filename}");

            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("$Resources");

                worksheet.Style.Font.FontName = "Meiryo";

                worksheet.Row(1).Style.Font.FontColor = XLColor.LightGray;
                worksheet.Row(1).Style.Fill.BackgroundColor = XLColor.Black;

                // header
                {
                    var row = 1;
                    var col = 1;

                    worksheet.Cell(row, col++).Value = "$Key";
                    foreach (var culture in _cultures)
                    {
                        worksheet.Cell(row, col++).Value = $"$.{culture}";
                    }
                    worksheet.Cell(row, col++).Value = "$Comment";
                }

                // body
                foreach (var item in _records.OrderBy(e => e.Key).Select((value, index) => new { value, index }))
                {
                    var key = item.value.Key;
                    var value = item.value.Value;

                    var row = item.index + 2;
                    var col = 1;
                    worksheet.Cell(row, col++).SetValue(key);

                    foreach (var culture in this._cultures)
                    {
                        if (value.Values.ContainsKey(culture))
                        {
                            worksheet.Cell(row, col).SetValue(value.Values[culture]);
                        }
                        col++;
                    }

                    worksheet.Cell(row, col++).SetValue(value.Comment);
                }

                var cols = this._cultures.Count + 2;
                worksheet.Columns(1, cols + 1).AdjustToContents();

                workbook.SaveAs(filename);
            }
        }

        //
        public void SaveResx(string filename, bool overwrite)
        {
            foreach (var culture in _cultures)
            {
                SaveResx(GetResxFilename(filename, culture), culture, overwrite || culture != DefaultCulture);
            }
        }

        //
        private void SaveResx(string filename, string culture, bool overwrite)
        {
            Console.WriteLine($"Save: {filename}");

            // ファイルが存在しないときは新規
            if (!File.Exists(filename)) overwrite = true;

            var nodeMap = (!overwrite ? LoadResxNodes(filename) : new List<ResXDataNode>()).ToDictionary(e => e.Name, e => e);

            //
            foreach (var record in this._records)
            {
                record.Value.Values.TryGetValue(culture, out string value);

                // 標準リソースでない場合は空白リソースは登録しない
                if (culture != DefaultCulture && string.IsNullOrWhiteSpace(value)) continue;

                // ノード登録
                ResXDataNode node = new ResXDataNode(record.Key, value ?? "");
                node.Comment = culture == ResourceMap.DefaultCulture ? record.Value.Comment : null;
                nodeMap[record.Key] = node;
            }

            //
            using (ResXResourceWriter rw = new ResXResourceWriter(filename))
            {
                foreach (var node in nodeMap.Values)
                {
                    rw.AddResource(node);
                }
                rw.Generate();
            }

            // アクセス日時を現在のローカル時刻で更新する
            //System.IO.File.SetLastAccessTime(filename, DateTime.Now);
        }


        // Resources.Designer.cs
        public void SaveResourceDesigner(string filename, string codeNamespace, bool overwrite)
        {
            var designer = Path.ChangeExtension(filename, ".Designer.cs");
            SaveResourcesDesigner(designer, filename, codeNamespace, overwrite);
        }

        private void SaveResourcesDesigner(string filename, string resxFile, string codeNamespace, bool overwrite)
        {
            Console.WriteLine($"Save: {filename}");

            var namespaceName = $"{codeNamespace}.Properties";
            var className = "Resources";
            var fileMode = overwrite ? FileMode.Create : FileMode.CreateNew;

            using (var stream = new FileStream(filename, fileMode, FileAccess.Write))
            using (var sw = new StreamWriter(stream, new UTF8Encoding(true)))
            {
                string[] errors = null;
                var provider = new CSharpCodeProvider();
                var code = StronglyTypedResourceBuilder.Create(resxFile, className, namespaceName, provider, false, out errors);

                if (errors.Length > 0)
                {
                    foreach (var error in errors)
                    {
                        Console.WriteLine(error);
                    }
                }

                provider.GenerateCodeFromCompileUnit(code, sw, new CodeGeneratorOptions());
            }
        }


        //
        private List<ResXDataNode> LoadResxNodes(string filename)
        {
            var nodes = new List<ResXDataNode>();

            using (ResXResourceReader rr = new ResXResourceReader(filename))
            {
                rr.UseResXDataNodes = true;
                IDictionaryEnumerator dict = rr.GetEnumerator();
                while (dict.MoveNext())
                {
                    ResXDataNode node = (ResXDataNode)dict.Value;
                    nodes.Add(node);
                }
            }

            return nodes;
        }
    }

}
