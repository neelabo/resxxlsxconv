﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResxXlsxConv
{
    class Program
    {
        static int Main(string[] args)
        {
            try
            {
                var process = new MainProcess();
                process.Execute(args);
                return 0;
            }
            catch(Exception ex)
            {
                Console.WriteLine("ERROR: " + ex.Message);
                return 1;
            }
        }
    }
}
